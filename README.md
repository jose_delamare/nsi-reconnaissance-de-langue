Ceci est un projet pour les élèves de NSI du Lycée Blaise Pascal de Rouen.

Le but est de trouver la langue utilisée dans le document, à partir de la fréquence d'apparition des lettres.

Il est composé de 5 parties : 

*  une introduction : elle permet de revoir : les affectations, les opérations sur les chaînes de caractères, le parcours séquentiel d'une chaîne de caractères, 
*  la partie_1 : on rédige 3 fonctions permettant de savoir si une lettre est présente, combien de fois elle est présente dans la chaîne de caractères, 
et combien de lettres au total comporte la chaîne de caractères.
*  la partie_2 : on définit une fonction permettant de calculer le pourcentage de chaque lettre dans une chaîne de caractères.
*  la partie_3 : il s'agit de revoir la structure d'un dictionnaire, puis d'en créer un à partir de la fréquence d'apparition de chaque caractère dans la chaîne.
*  la partie_4 : on réalise l'analyse proprement dite, en comparant les dictionnaires des différentes langages.


Vous devez impérativement commencer par l'introduction, puis chacune des parties, dans l'ordre où elles sont numérotées.

A la fin de chaque notebook, il faudra sauvegarder votre travail en cliquant sur File, puis Download as..., puis Notebook (.ipynb)
  